package lab2V4;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.Image;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class View {
	private JFrame frame;
	private JButton eatingButton, speakingButton, ichingButton, smellingButton;
	JTextField MessageTextField;
	JLabel label,thougths;
	JLabel ImageLabel;
	Image image;
	JPanel buttonsPanel;
	
	public View () {
		frame = new JFrame("Head");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(700, 500);
		frame.setBackground(Color.black);
		frame.setResizable(false);
		
		
		BorderLayout borderLayout = new BorderLayout();
		borderLayout.setHgap(10);
		borderLayout.setVgap(10);
		
		label = new JLabel("���������� ��������");
		frame.getContentPane().add(label,BorderLayout.PAGE_START );
		
		buttonsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		buttonsPanel.setBackground(Color.PINK);
		
		eatingButton = new JButton("����");
		buttonsPanel.add(eatingButton);
		
		speakingButton = new JButton("��������");
		buttonsPanel.add(speakingButton);
		
		ichingButton = new JButton("��������");
		buttonsPanel.add(ichingButton);
		
		smellingButton = new JButton("������");
		buttonsPanel.add(smellingButton);
		
		thougths = new JLabel("�����:");
		buttonsPanel.add(thougths);
		
		MessageTextField = new JTextField("                                             ");
		buttonsPanel.add(MessageTextField);
		frame.getContentPane().add(buttonsPanel,BorderLayout.PAGE_END);
		
		ImageLabel = new JLabel();
		ImageLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/patrick.gif")));
		
		frame.getContentPane().add(new JLabel("                             "),BorderLayout.LINE_START);
		frame.getContentPane().add(ImageLabel,BorderLayout.CENTER );
		
		
		frame.setVisible(true);
		
	}	
	void addEatingButtonListener (ActionListener listenToEatingButton) {
		eatingButton.addActionListener(listenToEatingButton);
	}
	void addSpeakingButtonListener (ActionListener listenToSpeakingButton) {
		speakingButton.addActionListener(listenToSpeakingButton);
	}
	void addIchingButtonListener (ActionListener listenToIchingButton) {
		ichingButton.addActionListener(listenToIchingButton);
	}
	void addSmellingButtonListener (ActionListener listenToSmellingButton) {
		smellingButton.addActionListener(listenToSmellingButton);
	}
	void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(frame, errorMessage);
	}

	
}
	