package lab2V4;

public class Brain extends Organ{
	private double efficiency;
	private Mouth mouthLink;
	private Nose noseLink;
	private String message;
	public Brain() {
		super();
		mouthLink = new Mouth();
		noseLink = new Nose();
	}
	public Brain(double efficiency, double weight, Mouth mouthLink,Nose noseLink) {
		super(weight);
		this.efficiency = efficiency;
		this.mouthLink = mouthLink;
		this.noseLink = noseLink;
	}
	public String Manager(ActionType action) {
		if(action == ActionType.Eat) {
			message = mouthLink.eat();
			return message;
		}
		if(action == ActionType.Smell) {
			message = noseLink.smell();
			return message;
			}
		else return message;
		//eat
	}
	@Override
	public OrganType getOrganType(){
		return OrganType.Brain;
	}
	public void getMouthMessage() {
		
	}
	public void getNoseMessage() {
		
	}
	public String getMessage() {
		return message;
	}
}
