package lab2V4;

public class Nose extends Organ {
	private String shape;
	private String message;
	public Nose(){
		super();
	}
	public Nose( double weight, String shape ){
		super(weight);
		this.shape = shape;
		
	}
	@Override
	public OrganType getOrganType(){
		return OrganType.Nose;
	}
	public String smell() {
		String message = "�����-�� �����                    ";
		return message;
	}
	public String itch() {
		String message = "��� �������!                       ";
		return message;
	}
	@Override
	public String getMessage() {
		return message;
	}
	@Override
	public String Manager(ActionType action) {
		if(action == ActionType.Ich) 
			return this.itch();
		else
			return message;
	}
}
