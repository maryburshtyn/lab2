package lab2V4;

public class Model {
	private Head head;
	
	
	public Model(){
		head = new Head();
	}
	
	public Head getHead() {
		return this.head;
	}
	public PictureAndMessage DispatchAction(ActionType action) {
		PictureAndMessage picAndMessage = new PictureAndMessage();
		switch(action) {
		  case Eat:
			  picAndMessage.message = ((Brain)head.getOrgan(OrganType.Brain)).Manager(ActionType.Eat);
			  picAndMessage.ImagePath = "images/eat.gif";
			  return picAndMessage;
		  case Smell:
			  picAndMessage.message = ((Brain)head.getOrgan(OrganType.Brain)).Manager(ActionType.Smell);
			  picAndMessage.ImagePath = "images/smell.gif";
			  return picAndMessage;
		  case Speak:
			  picAndMessage.message = ((Mouth)head.getOrgan(OrganType.Mouth)).Manager(ActionType.Speak);
			  picAndMessage.ImagePath = "images/talk.gif";
			  return picAndMessage;
		  case Ich:
			  picAndMessage.message = ((Nose)head.getOrgan(OrganType.Nose)).Manager(ActionType.Ich);
			  picAndMessage.ImagePath = "images/itch.gif";
			  return picAndMessage;
		
		
		}
		
		return picAndMessage;
	}
}
