package lab2V4;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {
	private Model theModel;
	private View theView;
	
	public Controller(Model theModel,View theView) {
		this.theModel = theModel;
		this.theView = theView;
		
		this.theView.addEatingButtonListener(new CommonListener(ActionType.Eat));
		this.theView.addSmellingButtonListener(new CommonListener(ActionType.Smell));
		this.theView.addIchingButtonListener(new CommonListener(ActionType.Ich));
		this.theView.addSpeakingButtonListener(new CommonListener(ActionType.Speak));
	}
	class CommonListener implements ActionListener{
		private ActionType actionType;
		public CommonListener(ActionType action)
		{
			actionType = action;
		}
		public void actionPerformed(ActionEvent e) {
			PictureAndMessage picAndMessage = theModel.DispatchAction(this.actionType);
			theView.MessageTextField.setText(picAndMessage.message);
			theView.ImageLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource(picAndMessage.ImagePath)));
		}
	}
	}

