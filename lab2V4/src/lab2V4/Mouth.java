package lab2V4;

public class Mouth extends Organ {
	private double size;
	private String message;
	public Mouth() {
		super();
		this.size = 0.0;
	}
	public Mouth(double size, double weight) {
		super(weight);
		this.size = size;
	}
	public String eat() {
		String message = "�������, ����� ������      ";
		return message;
	}
	public String speak() {
		String message = "� ���� ��������!                 ";
		return message;
	}
	public String Manager(ActionType action) {
		if(action == ActionType.Speak) 
			return this.speak();
		else
			return message;
	}
	@Override
	public OrganType getOrganType(){
		return OrganType.Mouth;
	}
	public String getMessage() {
		return message;
	}
}
